$(document).ready(function() {
	$('.btn-edit').on('click', function(){
		// $('input,select').attr('disabled','enable');
		// $("input,select").prop('disabled', false);

		$('.upload-image').toggleClass('hover');
		// $(this).toggleClass('edit');
		// 
		$(this).addClass('hide');
		$('.update').removeClass('hide').addClass('edit');

		$('input,select').each(function() {
            if ($(this).attr('disabled')) {
                $(this).removeAttr('disabled');
            }
            else {
                $(this).attr({
                    'disabled': 'disabled'
                });
            }
        });
	});

	$('.btn-update.update').on('click', function(){
		$(this).addClass('hide');
		$('.btn-edit').removeClass('hide').addClass('save');
		$('.notification-container').addClass('active');
		$('input,select').each(function() {
            if ($(this).attr('disabled')) {
                $(this).removeAttr('disabled');
            }
            else {
                $(this).attr({
                    'disabled': 'disabled'
                });
            }
        });

        setTimeout(function(){
			$('.btn-edit').removeClass('save');
			$('.notification-container').removeClass('active');

		}, 3000);

	});
});