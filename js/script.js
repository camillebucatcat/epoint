$(document).ready(function() {

	$('.clickable.go-to').on('click', function(){
		var target = $(this).data('target');
		var targetContent = $('.target-content').data('rel');

		$('.target-content:not(.modal-message)').addClass('hide');
		$('.target-content[data-rel="' + target + '"]').removeClass('hide').addClass('active');
	});

	$('.open-modal').on('click', function(){
		var target = $(this).data('target');

		$('.target-content[data-rel="' + target + '"]').removeClass('hide');
	});

	$('.btn-close').on('click', function(){
		var target = $(this).data('target');

		$(this).parents('.modal-message').addClass('hide').removeClass('active');
	});

	// $('.close-icon').on('click', function(){
	// 	$(this).parents('.menu').addClass('fadeOut');
	// 	$(this).parents('.menu div').addClass('slideOutLeft');
	// });

	$('[data-toggle]').on('click', function(){
		var $target = $($(this).data('toggle'));
		$target.toggleClass('active');
	});

	$('.btn-search').on('click', function(){
		$(this).parents('.sub-header-button').siblings('.search-box').toggleClass('active');
	});



	getCardInfoHeight();
	// getTabHeight();

	$(window).resize(function(){
		getCardInfoHeight();
		// getTabHeight();
	});

	function getCardInfoHeight() {		
		var windowWidth = $( window ).width();

		if(windowWidth >= 576) {
			var regCardHeight = $('.card-side-info').height();
			$('.card-side-image').css({'height': regCardHeight });
			console.log(regCardHeight);			
		}
	}

	function getTabHeight() {		
		var windowWidth = $( window ).width();

		if(windowWidth >= 850) {
			var tabContentHeight = $('.tabcontent').height();
			tabContentHeight = tabContentHeight.parseInt();
			tabContentHeight = tabContentHeight + 20;
			$('.tab-container').css({'height': tabContentHeight });
			$('.tab').css({'height': tabContentHeight });
			console.log(tabContentHeight);			
		}
	}

	$('#nav-icon').click(function(){
		$(this).toggleClass('open');
		$('.mobile-main-menu').toggleClass('open');
	});



	setTimeout(function(){
		$('.notification-container:not(.profile)').addClass('active');
	}, 300);

	setTimeout(function(){
		$('.notification-container:not(.profile)').removeClass('active');
	}, 5000);

	  window.asd = $('.SlectBox').SumoSelect({ csvDispCount: 3, selectAll:true, captionFormatAllSelected: "All", forceCustomRendering: true });
   $('.SlectBox').on('sumo:opened', function(o) {
      console.log("dropdown opened", o)
    });

	sliderInit();


   function sliderInit() {
		$('.bxslider').bxSlider({
		  // auto: true,
		  autoControls: true,
		  // stopAutoOnClick: true,
		  pager: true,
		  slideWidth: 600
		});
   }

   	$('.tablinks').on('click', function(){
        $('.tabcontent').css({
            'display' : 'none'
        });
        var tabLink = $(this).data('tab');
        console.log(tabLink);
        $('.tabcontent[id="'+ tabLink +'"]').css({
            'display' : 'block'
        });
    });

    $(document).ready(function(){
	  $(".tab-container a").on('click', function(event) {
	    if (this.hash !== "") {
	      event.preventDefault();

	      var hash = this.hash;

	      $('html, body').animate({
	        scrollTop: $(hash).offset().top
	      }, 800, function(){
	   
	        window.location.hash = hash;
	      });
	    } // End if
	  });
	});


	
   
});
